package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.Product;
import ru.pcs.web.models.User;
import ru.pcs.web.repositories.OrdersRepository;
import ru.pcs.web.repositories.UsersRepository;
import ru.pcs.web.services.ProductsService;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Controller
public class CatalogController {

    private final ProductsService productsService;
    private final OrdersRepository ordersRepository;
    private final UsersRepository usersRepository;

    @GetMapping("/catalog")
    public String getCatalogPage(ModelMap modelMap) {
        modelMap.put("products", productsService.getAllProducts());
        return "catalog";
    }

    @PostMapping("/catalog/add-to-order")
    public String addProductToOrder(@RequestParam("productsId") ArrayList<Integer> productsId,
                                @AuthenticationPrincipal(expression = "id") Integer currentUserId) {

        User user = usersRepository.getById(currentUserId);
        List<Order> orders = user.getOrders();
        List<Product> products = new ArrayList<>();
        Order order = new Order();
        for (Integer id : productsId) {
            products.add(productsService.getProduct(id));
        }
        order.setProducts(products);
        order.setUser(user);
        orders.add(order);
        user.setOrders(orders);
        usersRepository.save(user);
        ordersRepository.save(order);
        return "redirect:/profile/orders";
    }

}

