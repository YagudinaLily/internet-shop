package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.OrdersRepository;
import ru.pcs.web.repositories.ProductsRepository;
import ru.pcs.web.services.UsersService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Controller
public class ProfileController {

    private final UsersService usersService;
    private final OrdersRepository ordersRepository;
    private  final ProductsRepository productsRepository;

    @GetMapping("/profile")
    public String getProfilePage(@AuthenticationPrincipal(expression = "id") Integer currentUserId, Model model) {
        model.addAttribute("user", usersService.getUser(currentUserId));
        return "profile";
    }

    @GetMapping("/profile/orders")
    public String getOrdersByUser(@AuthenticationPrincipal(expression = "id") Integer currentUserId, Model model) {
        List<Order> orders = usersService.getOrdersByUserId(currentUserId);
        model.addAttribute("orders", orders);
        return "orders_of_user";
    }

    @GetMapping("orders/{order-id}")
    public String getOrderPage(ModelMap modelMap, @PathVariable("order-id") Integer orderId) {
        Order order = ordersRepository.getById(orderId);
        List<Product> products = order.getProducts();
        modelMap.put("products", products);
        return "order";
    }
}
