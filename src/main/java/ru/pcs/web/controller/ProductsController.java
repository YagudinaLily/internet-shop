package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.services.ProductsService;

import javax.validation.Valid;
import java.io.IOException;


@RequiredArgsConstructor
@Controller
public class ProductsController {

    private final ProductsService productsService;

    @GetMapping("/products")
    public String getProductsPage(ModelMap modelMap) {
        modelMap.put("products", productsService.getAllProducts());
        return "products";
    }

    @PostMapping("/products")
    public String addProduct(@Valid ProductForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Неправильно заполнена форма");
            return "redirect:/products";
        }
        productsService.addProduct(form);
        return "redirect:/products";
    }


    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productsService.deleteProduct(productId);
        return "redirect:/products";
    }

    @GetMapping("products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productsService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/products/{product-id}/update")
    public String productUpdate(ProductForm form, @PathVariable("product-id") Integer productId) {
        productsService.updateProduct(productId, form);
        return "redirect:/products";
    }
}
