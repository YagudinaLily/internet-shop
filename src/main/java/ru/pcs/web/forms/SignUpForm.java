package ru.pcs.web.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String email;
    private String password;

}
