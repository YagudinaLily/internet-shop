package ru.pcs.web.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.Product;
import ru.pcs.web.models.User;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ProductForm {
    private Integer id;
    @NotNull
    private String description;
    @NotNull
    private Double price;
    @NotNull
    private Integer amount;

}
