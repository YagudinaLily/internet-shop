package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.exceptions.UserNotFoundException;
import ru.pcs.web.forms.UserForm;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.User;
import ru.pcs.web.repositories.OrdersRepository;
import ru.pcs.web.repositories.UsersRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final OrdersRepository ordersRepository;

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {
        return usersRepository.findById(userId).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void update(Integer userId, UserForm userForm) {
        User user = usersRepository.findById(userId).orElseThrow(UserNotFoundException::new);
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        usersRepository.save(user);
    }

    @Override
    public List<Order> getOrdersByUserId(Integer userId) {
        return ordersRepository.findAllByUser_Id(userId);
    }

}
