package ru.pcs.web.services;

import ru.pcs.web.forms.UserForm;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.User;

import java.util.List;

public interface UsersService {

    List<Order> getOrdersByUserId(Integer userId);

    List<User> getAllUsers();

    void deleteUser(Integer userId);

    User getUser(Integer userId);

    void update(Integer userId, UserForm userForm);

}

