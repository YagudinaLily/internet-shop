package ru.pcs.web.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Order;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllByUser_Id(Integer userId);
}
