package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.Product;

import java.util.List;

public interface ProductsRepository extends JpaRepository<Product, Integer> {

}
