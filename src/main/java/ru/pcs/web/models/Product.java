package ru.pcs.web.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
public class Product {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="description")
    private String description;

    @Column(name="price")
    private Double price;

    @Column(name="amount")
    private Integer amount;

    @ManyToMany(mappedBy = "products")
    private List<Order> orders;

}
